﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TargetDetection : MonoBehaviour
{
    public UnityEvent<Player> OnPlayerHit;
    public UnityEvent<GameObject> OnBulletHit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Player>())
        {
            OnPlayerHit.Invoke(collision.gameObject.GetComponent<Player>());
        }
        else if(collision.gameObject.GetComponent<Bullet>() || collision.gameObject.GetComponent<Boomerang>())
        {
            Debug.Log(collision);
            OnBulletHit.Invoke(collision.gameObject);
        }
    }
}
