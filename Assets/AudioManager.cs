﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource audioSource;

    private void Update()
    {
        if(MenuManager.GameIsPaused)
        {
            audioSource.pitch *= .5f;
        }
        else
        {
            audioSource.pitch = 1f;
        }
    }
}
