﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AnimationManager : MonoBehaviour
{
    public UnityEvent OnAnimationEnd;
    public GameObject ObjectWithAnimation;
    //public float fadeOutTime;
    float t;

    //public Text text1;
    //public Text text2;
    void Start()
    {
        t = 0;
        Destroy(ObjectWithAnimation, ObjectWithAnimation.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
    }

    // Update is called once per frame
    void Update()
    {
        if (ObjectWithAnimation != null)
        {
            t += Time.deltaTime;
            if (t >= ObjectWithAnimation.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length)
            {
                OnAnimationEnd.Invoke();
            }
        }
    }

    //public void FadeOut()
    //{
    //    StartCoroutine(FadeOutRoutine(text1));
    //    StartCoroutine(FadeOutRoutine(text2));
    //}
    //private IEnumerator FadeOutRoutine(Text text)
    //{
    //    text = GetComponent<Text>();
    //    Color originalColor = text.color;
    //    for (float t = 0.01f; t < fadeOutTime; t += Time.deltaTime)
    //    {
    //        text.color = Color.Lerp(Color.clear, originalColor, Mathf.Min(1, t / fadeOutTime));
    //        yield return null;
    //    }
    //}
}
