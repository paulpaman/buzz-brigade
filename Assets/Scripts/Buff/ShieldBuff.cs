﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBuff : Buff
{
    GameObject shield;
    private void Awake()
    {

    }
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void BuffEffect()
    {
        base.BuffEffect();
        shield = Instantiate(player.shieldPrefab, player.shieldSpawnPos.transform.position, Quaternion.identity);
        shield.transform.SetParent(player.transform, false);
        shield.transform.localPosition = Vector3.zero;
        player.StartCoroutine(player.Invulnerable(duration));
        Color color = player.GetComponent<SpriteRenderer>().color;
        color.a = 1f;
        player.GetComponent<SpriteRenderer>().color = color;
    }
    protected override void OnBuffEnd()
    { 
        if(shield != null)
        {
            Destroy(shield);
        }
        base.OnBuffEnd();
    }

    public void EnableUI()
    {
        uiManager.EnableShield(duration);
    }

    public void DisableUI()
    {
        uiManager.DisableShotgun();
    }
}
