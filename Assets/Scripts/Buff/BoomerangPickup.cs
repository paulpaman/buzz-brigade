﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangPickup : Pickup
{
    public BoomerangBuff boomerangBuff;
    private float curDuration;
    protected override void Start()
    {
        base.Start();
    }
    protected override void Update()
    {
        base.Update();
    }

    protected override void OnPickup(Player player)
    {
        base.OnPickup(player);
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Buff");
        foreach (GameObject gameObj in objs)
        {
            player.curBulletData = player.normalBulletData;
            player.SetBoomerang(false);
            Destroy(gameObj);
        }
        BoomerangBuff buffObj = Instantiate(boomerangBuff, transform.position, Quaternion.identity);
        buffObj.duration = PickupDuration;
    }
}
