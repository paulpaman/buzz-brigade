﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield_Pickup : Pickup
{
    public ShieldBuff shieldBuff;
    private float curDuration;
    protected override void Start()
    {
        base.Start();
    }
    protected override void Update()
    {
        base.Update();
    }

    protected override void OnPickup(Player player)
    {
        base.OnPickup(player);
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Buff");
        foreach (GameObject gameObj in objs)
        {
            player.curBulletData = player.normalBulletData;
            player.SetBoomerang(false);
            Destroy(gameObj);
        }
        ShieldBuff buffObj = Instantiate(shieldBuff, transform.position, Quaternion.identity);
        buffObj.duration = PickupDuration;
    }
}
