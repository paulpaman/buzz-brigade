﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boomerang : MonoBehaviour
{
    public float Speed;
    public float MaxDistance;
    public float Damage;

    private Vector3 startPos;
    private bool isMaxDist;

    public GameObject ObjToGoBack;
    void Start()
    {
        isMaxDist = false;
        startPos = this.transform.position;
    }

    
    void Update()
    {
        float dist = Vector3.Distance(startPos, transform.position);
        Debug.Log(dist);
        if(isMaxDist == false)
        {
            transform.position += transform.right * Speed * Time.deltaTime;
        }
        else if (isMaxDist == true)
        {
            if(ObjToGoBack)
            {
                Vector2 curPos = new Vector2(transform.position.x, transform.position.y);
                Vector2 targetPos = new Vector2(ObjToGoBack.transform.position.x, ObjToGoBack.transform.position.y);
                transform.position = Vector2.MoveTowards(curPos, targetPos, Mathf.Abs(Speed) * Time.deltaTime);

                float distToObj = Vector3.Distance(ObjToGoBack.transform.position, transform.position);
                if (distToObj <= 0.5f)
                {
                    Destroy(this.gameObject);
                }
            }
        }

        if(dist >= MaxDistance)
        {
            isMaxDist = true;
        }

        if(ObjToGoBack == null)
        {
            Destroy(gameObject);
        }
    }
}
