﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Buff : MonoBehaviour
{
    public UnityEvent OnBuffActivate;
    public UnityEvent OnBuffDeactivate;
    public float duration;
    public float curDuration;
    protected Player player;

    protected UIManager uiManager;
    protected virtual void Start()
    {
        uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        player = GameObject.Find("Player").GetComponent<Player>();
        curDuration = duration;
        BuffEffect();
    }
    protected virtual void Update()
    {
        //Debug.Log(curDuration);
        curDuration -= Time.deltaTime;
        if(curDuration <= 0f)
        {
            OnBuffEnd();
            Destroy(this.gameObject);
        }
    }

    protected virtual void OnBuffEnd()
    {
        OnBuffDeactivate.Invoke();
    }

    protected virtual void BuffEffect()
    {
        OnBuffActivate.Invoke();
    }


}
