﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangBuff : Buff
{
    GameObject boomerang;
    private void Awake()
    {

    }
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void BuffEffect()
    {
        base.BuffEffect();
        player.SetBoomerang(true);
       
    }
    protected override void OnBuffEnd()
    {
        base.OnBuffEnd();
        player.SetBoomerang(false);
    }

    public void EnableUI()
    {
        uiManager.EnableBoomerang(duration);
    }

    public void DisableUI()
    {
        uiManager.DisableShotgun();
    }
}
