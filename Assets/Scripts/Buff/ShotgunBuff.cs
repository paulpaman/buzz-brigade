﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunBuff : Buff
{
    private void Awake()
    {
        
    }
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void BuffEffect()
    {
        base.BuffEffect();
        player.curBulletData = player.shotgunBulletData;
    }
    protected override void OnBuffEnd()
    {
        base.OnBuffEnd();
        player.curBulletData = player.normalBulletData;
    }

    public void EnableUI()
    {
        uiManager.EnableShotgun(duration);
    }

    public void DisableUI()
    {
        uiManager.DisableShotgun();
    }
}
