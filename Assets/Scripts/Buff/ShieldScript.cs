﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class ShieldScript : MonoBehaviour
{
    Player player;
    public UnityEvent<GameObject> OnEnemyHit;
    [SerializeField]Material material;

    Color originalColor;
    private void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        originalColor = material.color;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject gameObject = collision.gameObject;
        OnEnemyHit.AddListener(Hit);
        if (collision.gameObject.CompareTag("EnemyBullet")) OnEnemyHit.Invoke(gameObject);
    }

    public void Hit(GameObject gameObject)
    {
        material.SetColor("_Color", new Color(100, 100, 100));
        player.ShieldHit(gameObject);
        Invoke("ResetMaterial", 0.1f);
    }

    private void ResetMaterial()
    {
       material.color = originalColor;
    }
}
