﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class Player : MonoBehaviour
{
    public UnityEvent OnGetPickup;
    public UnityEvent OnPickupEnd;
    public UnityEvent OnPlayerDeath;

    public Camera MainCamera;
    public float Speed;
    public float MaxHealth;
    public float numOfHealth;
    public Material hitMaterial;
    public Material defaultMaterial;
    public Image[] hearts;
    public Sprite fullHeart;

    [SerializeField]private float Health;
 
    [Header("Bullet Data")]
    public BulletData curBulletData;
    public BulletData normalBulletData;
    public BulletData shotgunBulletData;


    [Header("Sound Data")]
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip waspFireClip;
    [SerializeField] AudioClip hitClip;

    [Header("Buff")]
    public GameObject shieldPrefab;
    public GameObject shieldSpawnPos;
    public GameObject BoomerangPrefab;

    [SerializeField] private bool canShoot;
    [SerializeField] float invulnerabilityTime;

    private bool isPickupCoroutineStarted;
    //[SerializeField] GameObject bulletSpawnPos;
    private float horizontalMovement;
    private float verticalMovement;

    private float fireRateCounter;
    private Vector2 screenBounds;
    private float objectWidth;
    private float objectHeight;
    private SpriteRenderer spriteRenderer;
    public new Collider2D collider;
    [SerializeField] CamAnim shake;

    public new Rigidbody2D rigidbody2D;

    public bool isInvincible;
    public GameObject BulletSpawnPos;

    bool isBoomerang;
    void Start()
    {
        isBoomerang = false;
        isInvincible = false;
        Health = MaxHealth;
        isPickupCoroutineStarted = false;
        curBulletData = normalBulletData;
        collider = GetComponent<Collider2D>();
        shake = shake.GetComponent<CamAnim>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        defaultMaterial = spriteRenderer.material;
        screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, MainCamera.transform.position.z));
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x;
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y;
        fireRateCounter = 0;
        canShoot = true;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        Shoot();
        FireRate();
        CheckForHearts();
        numOfHealth = Health;
    }

    private void LateUpdate()
    {
        Vector3 viewPos = transform.position;

        viewPos.x = Mathf.Clamp(viewPos.x, screenBounds.x* -1 + objectWidth, screenBounds.x - objectWidth);
        viewPos.y = Mathf.Clamp(viewPos.y, screenBounds.y * -1 + objectHeight, screenBounds.y - objectHeight);
        transform.position = viewPos;
    }


    private void Movement()
    {
        horizontalMovement = Input.GetAxis("Horizontal");
        verticalMovement = Input.GetAxis("Vertical");
        transform.position = transform.position + new Vector3(horizontalMovement * Speed * Time.deltaTime, verticalMovement * Speed * Time.deltaTime, 0);
    }

    private void Shoot()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (canShoot == true)
            {
                if(isBoomerang == false)
                {
                    //Debug.Log("Shoot");
                    SpawnBullet();
                }
                else
                {
                    if(isBoomerang == true)
                    {
                        ShootBoomerang();
                        audioSource.PlayOneShot(waspFireClip);
                    }
                }
                canShoot = false;
            }
        }
    }

    private void ShootBoomerang()
    {
        GameObject boomerang = Instantiate(BoomerangPrefab, BulletSpawnPos.transform.position, Quaternion.identity);
        boomerang.GetComponent<Boomerang>().ObjToGoBack = BulletSpawnPos;
    }
    
    private void FireRate()
    {
        if(canShoot == false)
        {
            fireRateCounter += 1*Time.deltaTime;
            if (fireRateCounter > curBulletData.fireRate)
            {
                fireRateCounter = 0;
                canShoot = true;
            }
        }
    }

    public void SpawnBullet()
    {
        float bulletLength = curBulletData.bulletsPerArrays - 1;
        if (bulletLength == 0)
        {
            bulletLength = 1;
        }

        float arrayLength = curBulletData.totalBulletArrays - 1 * curBulletData.totalBulletArrays;
        if (arrayLength == 0)
        {
            arrayLength = 1;
        }

        float arrayAngle = (curBulletData.spreadWithinArray / bulletLength); //Calculates the spread between each array
        float bulletAngle = (curBulletData.spreadBetweenArray / arrayLength); //Calcualtes the spread within the bullets in the arrays

        for (int i = 0; i < curBulletData.totalBulletArrays; i++)
        {
            for (int j = 0; j < curBulletData.bulletsPerArrays; j++)
            {
                Calculation(i, j, arrayAngle, bulletAngle);
            }
        }

        //If Default Angle > 360 , set it to 0
        float defaultAngle = curBulletData.defaultAngle;
        if (defaultAngle > 360)
        {
            defaultAngle = 0;
        }

        defaultAngle += curBulletData.spinRate; //Make the pattern spin

        float spinRate = curBulletData.spinRate;
        float spinModificator = curBulletData.spinModificator;
        spinRate += spinModificator; //Apply the spin modifier

        if (curBulletData.invertSpin == true)
        {
            if (spinRate < -curBulletData.maxSpinRate || spinRate > curBulletData.maxSpinRate)
            {
                spinModificator = -spinModificator;
            }
        }
    }
    private void Calculation(int i, int j, float arrayAngle, float bulletAngle)
    {
        float x1 = curBulletData.xOffset + lengthDirX(curBulletData.objectWidth, curBulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + curBulletData.startAngle);
        float y1 = curBulletData.yOffset + lengthDirY(curBulletData.objectWidth, curBulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + curBulletData.startAngle);

        float direction = curBulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + curBulletData.startAngle;

        //CreateBullet
        audioSource.PlayOneShot(waspFireClip);
        GameObject bullet = PlayerBulletPool.bulletPoolInstance.GetBullet();
        bullet.transform.position = new Vector3(this.transform.position.x + x1, this.transform.position.y + y1, 0);
        bullet.transform.rotation = transform.rotation;
        bullet.SetActive(true);
        bullet.GetComponent<Bullet>().SetBehaviors(x1, y1, direction, curBulletData.bulletSpeed, curBulletData.bulletAcceleration, curBulletData.bulletCurve, curBulletData.bulletTTL, curBulletData.Damage);
    }

    private float lengthDirX(float dist, float angle)
    {
        return dist * Mathf.Cos((angle * Mathf.PI) / 180);
    }

    private float lengthDirY(float dist, float angle)
    {
        return dist * -Mathf.Sin((angle * Mathf.PI) / 180);
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if(collision.gameObject.tag == "EnemyBullet")
    //    {
    //        TakeDamage();
    //        if(collision.gameObject.GetComponent<Bullet>() != null)
    //        {
    //            collision.gameObject.GetComponent<Bullet>().Destroy();
    //        }
    //    }
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(isInvincible == false)
        {
            if (collision.gameObject.tag == "EnemyBullet")
            {
                TakeDamage();
                if (collision.gameObject.GetComponent<Bullet>() != null)
                {
                    collision.gameObject.GetComponent<Bullet>().Destroy();
                }
            }
        }
    }


    private void ResetMaterial()
    {
        spriteRenderer.material = defaultMaterial;
    }

    public IEnumerator Invulnerable(float invulTime)
    {
        Color color = spriteRenderer.GetComponent<SpriteRenderer>().color;
        color.a = 0.5f;
        spriteRenderer.GetComponent<SpriteRenderer>().color = color;
        isInvincible = true;
        //collider.enabled = false;
        yield return new WaitForSeconds(invulTime);

        color.a = 1f;
        spriteRenderer.GetComponent<SpriteRenderer>().color = color;
        isInvincible = false;
        //collider.enabled = true;
    }

    public void TakeDamage()
    {
        audioSource.PlayOneShot(hitClip);
        shake.CamShake();
        spriteRenderer.material = hitMaterial;
        Health -= 1;
        
        if(Health <= 0)
        {
            OnPlayerDeath.Invoke();
            Destroy(this.gameObject);
        }
        else
        {
            Invoke("ResetMaterial", 0.1f);
            StartCoroutine(Invulnerable(invulnerabilityTime));
        }


    }

    public void ShotgunUpgrade()
    {
        curBulletData = shotgunBulletData;
        OnGetPickup.Invoke();
    }

    public void AddHealth()
    {
        if(Health < MaxHealth)
        {
            Health += 1;
        }
    }

    public void CheckForHearts()
    {
        for (int i = 0; i < hearts.Length; i++)
        {
            if(i < numOfHealth)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
    }

    public IEnumerator StartPowerUpTimer(float timer)
    {
        Debug.Log("Powerup Countdown");
        yield return new WaitForSeconds(timer);

        if(isPickupCoroutineStarted == false)
        {
            curBulletData = normalBulletData;
            OnPickupEnd.Invoke();
            isPickupCoroutineStarted = false;
        }
        else
        {
            yield break;
        }
    }

    public void ShieldHit(GameObject gameObj)
    {
        Debug.Log("Shield Hit");
        GameObject hit = gameObj;
        if (hit.GetComponent<Bullet>() != null)
        {
            hit.GetComponent<Bullet>().Destroy();
        }
        else
        {
            Destroy(gameObj);
        }
    }

    public void SetBoomerang(bool b)
    {
        isBoomerang = b;
    }

}
