﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class SceneLoader : MonoBehaviour
{
    public UnityEvent OnThisEnable;

    private void OnEnable()
    {
        OnThisEnable.Invoke();
    }
}
