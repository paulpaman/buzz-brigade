﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    public Animator transition;
    public float transitionTime = 1f;

    [SerializeField] AudioSource audioSource;
    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayLevel1()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }
    public void PlayLevel2()
    {
        if(PlayerPrefs.GetInt("LevelsFinished", 0) > 0)
        {
            StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 3));
        }
    }
    public void PlayLevel3()
    {
        if (PlayerPrefs.GetInt("LevelsFinished", 0) > 1)
        {
            StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 4));
        }    
    }

    public void PlayMainMenu()
    {
         StartCoroutine(LoadMainMenu());

    }

    IEnumerator LoadLevel(int levelIndex)
    {
        transition.SetTrigger("Start");
        audioSource.Play(0);
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(levelIndex);
    }

    IEnumerator LoadMainMenu()
    {
        transition.SetTrigger("Start");
        audioSource.Play(0);
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("MainMenu");
    }
}
