﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePrefs : MonoBehaviour
{
    private static SavePrefs _instance;
    public static SavePrefs Instance { get { return _instance; } }
    int levelsFinished;
    int isShotgunPickupUnlocked = 0;
    int isShieldPickupUnlocked = 0;
    int isBoomerangPickupUnlocked = 0;
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
    public void SaveLevel(int levelToSave)
    {
        PlayerPrefs.SetInt("LevelsFinished", levelToSave);
        Debug.Log(PlayerPrefs.GetInt("LevelsFinished", 0));
    }

    public void UnlockShotgunPickup()
    {
        isShotgunPickupUnlocked = 1;
    }

    public void UnlockShieldPickup()
    {
        isShieldPickupUnlocked = 1;
    }

    public void UnlockBoomerangPickup()
    {
        isBoomerangPickupUnlocked = 1;
    }

    public void SetLevel1HiScore(int score)
    {
        PlayerPrefs.SetInt("Level1HighScore", score);
    }
    public void SetLevel2HiScore(int score)
    {
        PlayerPrefs.SetInt("Level2HighScore", score);
    }
    public void SetLevel3HiScore(int score)
    {
        PlayerPrefs.SetInt("Level3HighScore", score);
    }

    public int GetLevel1HiScore()
    {
       int score =  PlayerPrefs.GetInt("Level1HighScore", 0);
       return score;
    }

    public int GetLevel2HiScore()
    {
        int score =  PlayerPrefs.GetInt("Level2HighScore", 0);
        return score;
    }

    public int GetLevel3HiScore()
    {
        int score  = PlayerPrefs.GetInt("Level3HighScore", 0);
        return score;
    }

    public void SaveLevel1()
    {

    }

    public void SaveLevel2()
    {

    }

    public void SaveLevel3()
    {

    }

}
