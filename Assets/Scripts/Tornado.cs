﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tornado : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float playerThrust;


    private void Start()
    {
        
    }
    void Update()
    {
        transform.Translate(-Vector2.right * speed * Time.deltaTime);
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if(collision.gameObject.CompareTag("Player"))
    //    {
    //        PushPlayer(collision.gameObject.GetComponent<Player>());
    //        Destroy(this.gameObject);
    //    }
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PushPlayer(collision.gameObject.GetComponent<Player>());
            Destroy(this.gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }

    void PushPlayer(Player player)
    {
        Vector3 dir = this.transform.position - player.transform.position;
        dir = -dir.normalized;
        player.GetComponent<Rigidbody2D>().AddForce((dir * playerThrust), ForceMode2D.Impulse);
    }

}
