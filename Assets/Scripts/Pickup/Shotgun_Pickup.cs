﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun_Pickup : Pickup
{
    public ShotgunBuff ShotgunBuff;
    private float curDuration;
    protected override void Start()
    {
        base.Start();
    }
    protected override void Update()
    {
        base.Update();
    }

    protected override void OnPickup(Player player)
    {
        base.OnPickup(player);
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Buff");
        foreach(GameObject gameObj in objs)
        {
            player.curBulletData = player.normalBulletData;
            player.SetBoomerang(false);
            Destroy(gameObj);
        }
        ShotgunBuff buffObj = Instantiate(ShotgunBuff, transform.position, Quaternion.identity);
        buffObj.duration = PickupDuration;
    }
}
