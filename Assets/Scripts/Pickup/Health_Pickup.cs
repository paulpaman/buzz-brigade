﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health_Pickup : Pickup
{
    protected override void OnPickup(Player player)
    {
        base.OnPickup(player);
        player.AddHealth();
    }
}
