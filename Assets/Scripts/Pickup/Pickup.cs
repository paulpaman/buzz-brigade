﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    [SerializeField] protected float PickupDuration;
    [SerializeField] protected float LifeDuration;

    private Collider2D collider2d;

    protected virtual void Start()
    {
        collider2d = gameObject.GetComponent<Collider2D>();
    }

    protected virtual void Update()
    {
        Destroy(gameObject, LifeDuration);
    }
    protected virtual void OnPickup(Player player)
    {

    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Player"))
    //    {
    //        Player player = collision.gameObject.GetComponent<Player>();
    //        OnPickup(player);
    //        Destroy(this.gameObject);
    //        //StartCoroutine(Die());
    //    }
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Player player = collision.gameObject.GetComponent<Player>();
            OnPickup(player);
            Destroy(this.gameObject);
            //StartCoroutine(Die());
        }
    }

    IEnumerator Die()
    {
        collider2d.enabled = false;
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);

        yield break;
    }
}
