﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    [SerializeField] private BulletData bulletData;

    private bool canShoot;
    private float fireRateCounter;
    void Start()
    {
        fireRateCounter = 0;
        canShoot = true;
    }

    
    void Update()
    {
        Shoot();
        FireRate();
    }

    private void Shoot()
    {
        if (canShoot == true)
        {
            SpawnBullet();
            canShoot = false;
        }
    }

    private void FireRate()
    {
        if (canShoot == false)
        {
            fireRateCounter += 1 * Time.deltaTime;
            if (fireRateCounter > bulletData.fireRate)
            {
                fireRateCounter = 0;
                canShoot = true;
            }
        }
    }

    public void SpawnBullet()
    {
        float bulletLength = bulletData.bulletsPerArrays - 1;
        if (bulletLength == 0)
        {
            bulletLength = 1;
        }

        float arrayLength = bulletData.totalBulletArrays - 1 * bulletData.totalBulletArrays;
        if (arrayLength == 0)
        {
            arrayLength = 1;
        }

        float arrayAngle = (bulletData.spreadWithinArray / bulletLength); //Calculates the spread between each array
        float bulletAngle = (bulletData.spreadBetweenArray / arrayLength); //Calcualtes the spread within the bullets in the arrays

        for (int i = 0; i < bulletData.totalBulletArrays; i++)
        {
            for (int j = 0; j < bulletData.bulletsPerArrays; j++)
            {
                Calculation(i, j, arrayAngle, bulletAngle);
            }
        }

        //If Default Angle > 360 , set it to 0
        float defaultAngle = bulletData.defaultAngle;
        if (defaultAngle > 360)
        {
            defaultAngle = 0;
        }

        defaultAngle += bulletData.spinRate; //Make the pattern spin

        float spinRate = bulletData.spinRate;
        float spinModificator = bulletData.spinModificator;
        spinRate += spinModificator; //Apply the spin modifier

        if (bulletData.invertSpin == true)
        {
            if (spinRate < -bulletData.maxSpinRate || spinRate > bulletData.maxSpinRate)
            {
                spinModificator = -spinModificator;
            }
        }
    }

    private void Calculation(int i, int j, float arrayAngle, float bulletAngle)
    {
        float x1 = bulletData.xOffset + lengthDirX(bulletData.objectWidth, bulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + bulletData.startAngle);
        float y1 = bulletData.yOffset + lengthDirY(bulletData.objectWidth, bulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + bulletData.startAngle);

        float direction = bulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + bulletData.startAngle;

        //CreateBullet
        GameObject bullet = EnemyBulletPool.bulletPoolInstance.GetBullet();
        bullet.transform.position = new Vector3(this.transform.position.x + x1, this.transform.position.y + y1, 0);
        bullet.transform.rotation = transform.rotation;
        bullet.SetActive(true);
        bullet.GetComponent<Bullet>().SetBehaviors(x1, y1, direction, bulletData.bulletSpeed, bulletData.bulletAcceleration, bulletData.bulletCurve, bulletData.bulletTTL, bulletData.Damage);
    }

    private float lengthDirX(float dist, float angle)
    {
        return dist * Mathf.Cos((angle * Mathf.PI) / 180);
    }

    private float lengthDirY(float dist, float angle)
    {
        return dist * -Mathf.Sin((angle * Mathf.PI) / 180);
    }
}
