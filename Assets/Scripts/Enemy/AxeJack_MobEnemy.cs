﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeJack_MobEnemy : MobEnemy
{
    Vector2 randomLoc;
    float timer;
    [SerializeField] float shootTimer;
    [SerializeField] float minumumMovementTimer;
    [SerializeField] float maximumMovementTimer;
    private float MoveTimer;
    Animator animator;
    public GameObject axe;

    protected override void Start()
    {
        base.Start();
        MoveTimer = Random.Range(minumumMovementTimer, maximumMovementTimer);
        randomLoc = new Vector2(Random.Range(movementBounds.bounds.min.x, movementBounds.bounds.max.x), Random.Range(movementBounds.bounds.min.y, movementBounds.bounds.max.y));
        InvokeRepeating("ChangeRandomLoc", 0, MoveTimer);
        animator = GetComponent<Animator>();
    }

    protected override void Update()
    {
        base.Update();
        Move();
    }

    protected override void Move()
    {
        base.Move();
        transform.localPosition = Vector2.MoveTowards(transform.localPosition, randomLoc, Time.deltaTime * speed);
        Vector3 endPos = new Vector3(randomLoc.x, randomLoc.y, 0);
        if(this.transform.localPosition == endPos)
        {
            timer += Time.deltaTime;
            if(timer >= shootTimer)
            {
                Shoot();
                timer = 0f;
            }
        }
    }

    protected override void Shoot()
    {
        base.Shoot();

        ThrowAxe();
    }

    protected override void Die()
    {
        base.Die();
    }

    protected override void OnPlayerCollision(Player player)
    {
        base.OnPlayerCollision(player);
        
    }

    protected override void PlaySound()
    {
        base.PlaySound();
    }

    void ChangeRandomLoc()
    {
        MoveTimer = Random.Range(minumumMovementTimer, maximumMovementTimer);
        randomLoc = new Vector2(Random.Range(movementBounds.bounds.min.x, movementBounds.bounds.max.x), Random.Range(movementBounds.bounds.min.y, movementBounds.bounds.max.y));
    }

    public void SpawnBullet()
    {
        PlaySound();
        GameObject boomerang = Instantiate(axe, BulletSpawnPosition.transform.position, Quaternion.identity);
        boomerang.GetComponent<Boomerang>().ObjToGoBack = BulletSpawnPosition;
    }

    void ThrowAxe()
    {
        animator.SetTrigger("Throw");
    }
}
