﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suicide_MobEnemy : MobEnemy
{
    Vector2 randomLoc;
    Player player;
    float dist;
    float suicideSpeed;
    [SerializeField] float speedModifier;
    bool shouldMove;
    Color originalColor;
    public GameObject ExplosionPrefab;
    private bool hasExploded;
    new Collider2D collider;
    protected override void Start()
    {
        base.Start();

        hasExploded = false;
        collider = gameObject.GetComponent<Collider2D>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        randomLoc = new Vector2(Random.Range(movementBounds.bounds.min.x, movementBounds.bounds.max.x), Random.Range(movementBounds.bounds.min.y, movementBounds.bounds.max.y));
        suicideSpeed = speed * speedModifier;
        shouldMove = true;
        originalColor = spriteRenderer.material.color;
    }

    protected override void Update()
    {
        base.Update();

        if (player)
        {
            dist = Vector3.Distance(player.transform.position, transform.position);
            //Debug.Log(dist);
            if (shouldMove == true)
            {
                Move();
            }

            if (dist < 1.5f)
            {
                shouldMove = false;
                StartCoroutine(Explode());
            }
        }
    }

    protected override void Die()
    {
        base.Die();
    }

    protected override void Move()
    {
        base.Move();

        transform.localPosition = Vector2.MoveTowards(transform.localPosition, randomLoc, Time.deltaTime * speed);
        Vector3 endPos = new Vector3(randomLoc.x, randomLoc.y, 0);
        if (this.transform.localPosition == endPos)
        {
            StartCoroutine(GoToPlayer());
        }
    }

    protected override void OnPlayerCollision(Player player)
    {
        base.OnPlayerCollision(player);
        StartCoroutine(Explode());
    }

    IEnumerator GoToPlayer()
    {
        yield return new WaitForSeconds(2f);

        speed = suicideSpeed;
        Debug.Log("Move To Player");
        Vector3 playerPos = GetPlayerPos();
        randomLoc = playerPos;
    }

    IEnumerator Explode()
    {
        StartCoroutine(Blink());
        shouldMove = false;
        yield return new WaitForSeconds(1f);
        // this.gameObject.SetActive(false);
        collider.enabled = false;
        if(hasExploded == false)
        {
            GameObject explosion = Instantiate(ExplosionPrefab, transform.position, Quaternion.identity);
            hasExploded = true;
        }
        yield return new WaitForSeconds(0.1f);
        Die();

        yield break;
    }

    IEnumerator Blink()
    {
        while(true)
        {
            spriteRenderer.material.color = Color.red;
            PlaySound();
            yield return new WaitForSeconds(0.2f);

            spriteRenderer.material.color = Color.white;
            PlaySound();
            yield return new WaitForSeconds(0.2f);
        }
    }

    Vector3 GetPlayerPos()
    {
        return player.transform.position;
    }
}
