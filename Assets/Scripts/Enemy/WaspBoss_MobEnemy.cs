﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class WaspBoss_MobEnemy : MobEnemy
{
    Vector2 randomLoc;
    Animator animator;
    float timer;
    [SerializeField] float shootTimer;
    [SerializeField] BulletData bulletData;
    [SerializeField] float minumumMovementTimer;
    [SerializeField] float maximumMovementTimer;
    [SerializeField] float waveSpawnWaitTime;
    [SerializeField] float enemySpawnWaitTime;
    //[SerializeField] int shotsPerAction;
    [SerializeField] BulletData[] bulletArray;
    private float MoveTimer;
    int randChance;
    public GameObject SpawnPos;

    [SerializeField] GameObject[] mobList;

    [Header("Debug")]
    [SerializeField] int enemiesKilled;
    [SerializeField] int totalEnemies;
    [SerializeField] bool canSpawn;
    [SerializeField] bool coroutineAllowed;
    [SerializeField] bool actionCoroutineAllowed;

    [Header("AdditionalAudio")]
    [SerializeField] AudioClip summonClip;


    int randBullet;   

    protected override void Start()
    {
        base.Start();

        randBullet = Random.Range(0, bulletArray.Length);
        bulletData = bulletArray[randBullet];
        actionCoroutineAllowed = true;
        coroutineAllowed = true;
        canSpawn = true;
        MoveTimer = Random.Range(minumumMovementTimer, maximumMovementTimer);
        randomLoc = new Vector2(Random.Range(movementBounds.bounds.min.x / 2, movementBounds.bounds.max.x), Random.Range(movementBounds.bounds.min.y, movementBounds.bounds.max.y));
        InvokeRepeating("ChangeRandomLoc", 0, MoveTimer);

        animator = this.GetComponent<Animator>();
    }

    protected override void Update()
    {
        base.Update();
        Move();
    }

    protected override void Shoot()
    {
        base.Shoot();
        PlaySound();
        SpawnBullet();
    }

    protected override void Die()
    {
        base.Die();
    }

    protected override void OnPlayerCollision(Player player)
    {
        base.OnPlayerCollision(player);
    }

    protected override void Move()
    {
        base.Move();
        transform.localPosition = Vector2.MoveTowards(transform.localPosition, randomLoc, Time.deltaTime * speed);
        Vector3 endPos = new Vector3(randomLoc.x, randomLoc.y, 0);
        if (this.transform.localPosition == endPos)
        {
            if(actionCoroutineAllowed)
            {
                StartCoroutine(Action());
            }
        }
    }

    protected override void PlaySound()
    {
        base.PlaySound();
    }

    void ChangeRandomLoc()
    {
        MoveTimer = Random.Range(minumumMovementTimer, maximumMovementTimer);
        randomLoc = new Vector2(Random.Range(movementBounds.bounds.min.x, movementBounds.bounds.max.x), Random.Range(movementBounds.bounds.min.y, movementBounds.bounds.max.y));
    }

    public void SpawnBullet()
    {
        float bulletLength = bulletData.bulletsPerArrays - 1;
        if (bulletLength == 0)
        {
            bulletLength = 1;
        }

        float arrayLength = bulletData.totalBulletArrays - 1 * bulletData.totalBulletArrays;
        if (arrayLength == 0)
        {
            arrayLength = 1;
        }

        float arrayAngle = (bulletData.spreadWithinArray / bulletLength); //Calculates the spread between each array
        float bulletAngle = (bulletData.spreadBetweenArray / arrayLength); //Calcualtes the spread within the bullets in the arrays

        for (int i = 0; i < bulletData.totalBulletArrays; i++)
        {
            for (int j = 0; j < bulletData.bulletsPerArrays; j++)
            {
                Calculation(i, j, arrayAngle, bulletAngle);
            }
        }

        //If Default Angle > 360 , set it to 0
        float defaultAngle = bulletData.defaultAngle;
        if (defaultAngle > 360)
        {
            defaultAngle = 0;
        }

        defaultAngle += bulletData.spinRate; //Make the pattern spin

        float spinRate = bulletData.spinRate;
        float spinModificator = bulletData.spinModificator;
        spinRate += spinModificator; //Apply the spin modifier

        if (bulletData.invertSpin == true)
        {
            if (spinRate < -bulletData.maxSpinRate || spinRate > bulletData.maxSpinRate)
            {
                spinModificator = -spinModificator;
            }
        }
    }

    private void Calculation(int i, int j, float arrayAngle, float bulletAngle)
    {
        float x1 = bulletData.xOffset + lengthDirX(bulletData.objectWidth, bulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + bulletData.startAngle);
        float y1 = bulletData.yOffset + lengthDirY(bulletData.objectWidth, bulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + bulletData.startAngle);

        float direction = bulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + bulletData.startAngle;

        //CreateBullet
        GameObject bullet = EnemyBulletPool.bulletPoolInstance.GetBullet();
        bullet.transform.position = new Vector3(BulletSpawnPosition.transform.position.x + x1, BulletSpawnPosition.transform.position.y + y1); //new Vector3(this.transform.position.x + x1, this.transform.position.y + y1, 0);
        bullet.transform.rotation = transform.rotation;

        bullet.GetComponent<Bullet>().SetBehaviors(x1, y1, direction, bulletData.bulletSpeed, bulletData.bulletAcceleration, bulletData.bulletCurve, bulletData.bulletTTL, bulletData.Damage);

        bullet.SetActive(true);
    }

    private float lengthDirX(float dist, float angle)
    {
        return dist * Mathf.Cos((angle * Mathf.PI) / 180);
    }

    private float lengthDirY(float dist, float angle)
    {
        return dist * -Mathf.Sin((angle * Mathf.PI) / 180);
    }

    IEnumerator Action()
    {
        actionCoroutineAllowed = false;

        randChance = Random.Range(0, 100);
        if(randChance<=25)
        {
            if(coroutineAllowed == true && canSpawn == true)
            StartCoroutine(SpawnWave());
        }
        else
        {
            randBullet = Random.Range(0, bulletArray.Length);
            bulletData = bulletArray[randBullet];
            StartCoroutine(ShootEvent());
        }

        yield return new WaitForSeconds(5f);
        actionCoroutineAllowed = true;
    }

    IEnumerator ShootEvent()
    {
        for(int i = 0; i< bulletData.shotsPerAction; i++)
        {
            Shoot();
            yield return new WaitForSeconds(bulletData.fireRate);
        }
    }
    IEnumerator SpawnWave()
    {
        coroutineAllowed = false;
        animator.SetTrigger("SummonTrigger");
        audioSource.PlayOneShot(summonClip);

        yield return new WaitForSeconds(waveSpawnWaitTime);
        for (int i = 0; i < mobList.Length; i++)
        {
            SpawnEnemy(mobList[i].GetComponent<MobEnemy>());

            yield return new WaitForSeconds(enemySpawnWaitTime);
        }

        canSpawn = false;

        yield return new WaitForSeconds(5f);

        coroutineAllowed = true;
    }

    void SpawnEnemy(MobEnemy enemyToSpawn)
    {
        MobEnemy enemy = Instantiate(enemyToSpawn, SpawnPos.transform.position, Quaternion.identity);
        enemy.OnDeath.AddListener(OnEnemyKill);
    }

    void OnEnemyKill()
    {
        enemiesKilled++;

        if (enemiesKilled >= totalEnemies)
        {
            canSpawn = true;
            enemiesKilled = 0;
        }
    }
}
