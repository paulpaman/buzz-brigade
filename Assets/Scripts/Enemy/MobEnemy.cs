﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class MobEnemy : MonoBehaviour
{
    public UnityEvent OnBossDeath;

    [SerializeField] protected float speed;
    [SerializeField] protected Material hitMaterial;

    [Header("SoundData")]
    [SerializeField] AudioClip audioClip;
    [SerializeField] protected AudioSource audioSource;

    protected BoxCollider2D movementBounds;
    private Material defaultMaterial;
    protected SpriteRenderer spriteRenderer;
    public Health health;
    public Score scoreScript;
    public UnityEvent OnDeath;
    public bool isBoss;
    [SerializeField] protected GameObject BulletSpawnPosition;
    [SerializeField] GameObject SmokePrefab;
    bool hasDied;

    protected bool isInvulnerable;
    protected virtual void Start()
    {
        hasDied = false;
        isInvulnerable = false;
        spriteRenderer = GetComponent<SpriteRenderer>();
        defaultMaterial = spriteRenderer.material;

        movementBounds = GameObject.FindGameObjectWithTag("EnemyBounds").GetComponent<BoxCollider2D>();
        audioSource = GameObject.Find("SFX").GetComponent<AudioSource>();
    }


    protected virtual void Update()
    {

    }

    protected virtual void Move()
    {

    }

    protected virtual void Shoot()
    {

    }

    protected virtual void Die()
    {
        if (isBoss)
        {
            OnBossDeath.Invoke();
        }
        OnDeath.Invoke();
        if (SmokePrefab)
        {
            GameObject smoke = Instantiate(SmokePrefab, transform.position, Quaternion.identity);
        }
        Destroy(this.gameObject);
    }

    protected virtual void OnPlayerCollision(Player player)
    {
        player.TakeDamage();
    }

    protected virtual void PlaySound()
    {
        audioSource.PlayOneShot(audioClip);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(isInvulnerable == false)
        {
            if (hasDied == false)
            {
                if (collision.gameObject.CompareTag("PlayerBullet"))
                {
                    Bullet bullet = collision.gameObject.GetComponent<Bullet>();
                    Boomerang boomerang = collision.gameObject.GetComponent<Boomerang>();
                    if (bullet)
                    {
                        health.TakeDamage(bullet.damage);
                    }
                    else if (boomerang)
                    {
                        health.TakeDamage(boomerang.Damage);
                    }
                    //health.TakeDamage(collision.gameObject.GetComponent<Bullet>().damage);
                    spriteRenderer.material = hitMaterial;
                    if (bullet)
                    {
                        collision.gameObject.GetComponent<Bullet>().Destroy();
                    }

                    if (health.currentHealth <= 0f)
                    {
                        hasDied = true;
                        Die();
                        scoreScript = FindObjectOfType<Score>();
                        scoreScript.AddScore(health.maxHealth);
                    }
                    else
                    {
                        Invoke("ResetMaterial", 0.1f);
                    }
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hasDied == false)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Player player = collision.gameObject.GetComponent<Player>();
                if (player.isInvincible == false)
                {
                    OnPlayerCollision(player);
                }
            }
        }
    }

    private void ResetMaterial()
    {
        spriteRenderer.material = defaultMaterial;
    }

    public void DamagePlayer(Player player)
    {
        if (player.isInvincible == false)
        {
            OnPlayerCollision(player);
        }
    }

    public void TakeDamage(GameObject collision)
    {
        if(isInvulnerable == false)
        {
            if (hasDied == false)
            {
                if (collision.gameObject.CompareTag("PlayerBullet"))
                {
                    Bullet bullet = collision.gameObject.GetComponent<Bullet>();
                    Boomerang boomerang = collision.gameObject.GetComponent<Boomerang>();
                    if (bullet)
                    {
                        health.TakeDamage(bullet.damage);
                    }
                    else if (boomerang)
                    {
                        health.TakeDamage(boomerang.Damage);
                    }
                    //health.TakeDamage(collision.gameObject.GetComponent<Bullet>().damage);
                    spriteRenderer.material = hitMaterial;
                    if (bullet)
                    {
                        collision.gameObject.GetComponent<Bullet>().Destroy();
                    }

                    if (health.currentHealth <= 0f)
                    {
                        hasDied = true;
                        Die();
                        scoreScript = FindObjectOfType<Score>();
                        scoreScript.AddScore(health.maxHealth);
                    }
                    else
                    {
                        Invoke("ResetMaterial", 0.1f);
                    }
                }
            }
        }
        
    }

    public void DestroyBullet(GameObject collision)
    {
        if (collision.gameObject.CompareTag("PlayerBullet"))
        {
            Bullet bullet = collision.gameObject.GetComponent<Bullet>();
            Boomerang boomerang = collision.gameObject.GetComponent<Boomerang>();
            if (bullet)
            {
                collision.gameObject.GetComponent<Bullet>().Destroy();
            }

            if (boomerang)
            {
                Destroy(collision);
            }
        }
    }
}
