﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Drop Data", menuName = "Drop Data")]

public class EnemyDropData : ScriptableObject
{
    public GameObject HealthDrop;
    public GameObject ShotgunDrop;
    public GameObject ShieldDrop;
    public GameObject BoomerangDrop;
}
