﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiggerJack_Enemy : MobEnemy
{
    Vector2 randomLoc;
    Animator animator;

    public GameObject WarningPrefab;
    [SerializeField] float minumumMovementTimer;
    [SerializeField] float maximumMovementTimer;

    private float MoveTimer;
    int randChance;

    [SerializeField] GameObject logPrefab;
    [SerializeField] GameObject logThrowPrefab;
    [SerializeField] GameObject logThrowSpawnPos;

    [Header("Debug")]
    [SerializeField] bool actionCoroutineAllowed;

    [Header("AdditionalAudio")]
    [SerializeField] AudioClip warningClip;
    [SerializeField] AudioClip logFallClip;
    [SerializeField] AudioClip logThrowClip;
    [SerializeField] AudioClip logRollingClip;

    Player player;
    
    protected override void Start()
    {
        base.Start();
        actionCoroutineAllowed = true;
        MoveTimer = Random.Range(minumumMovementTimer, maximumMovementTimer);
        randomLoc = new Vector2(Random.Range(movementBounds.bounds.min.x / 2, movementBounds.bounds.max.x), Random.Range(movementBounds.bounds.min.y, movementBounds.bounds.max.y));
        InvokeRepeating("ChangeRandomLoc", 0, MoveTimer);

        animator = this.GetComponent<Animator>();
        player = GameObject.Find("Player").GetComponent<Player>();
    }

    protected override void Update()
    {
        base.Update();

        Move();

        
    }

    protected override void Move()
    {
        base.Move();
        transform.localPosition = Vector2.MoveTowards(transform.localPosition, randomLoc, Time.deltaTime * speed);
        Vector3 endPos = new Vector3(randomLoc.x, randomLoc.y, 0);
        if (this.transform.localPosition == endPos)
        {
            //Debug.Log("action");
            if (actionCoroutineAllowed)
            {
                StartCoroutine(Action());
            }
        }
    }

    protected override void PlaySound()
    {
        base.PlaySound();
    }

    IEnumerator Action()
    {
        actionCoroutineAllowed = false;

        int random = Random.Range(0, 100);
        Debug.Log(random);
        if (random < 45f)
        {
            LogFall();
        }
        else if (random < 90f)
        {
            LogThrow();
        }
        else
        {

        }

        yield return new WaitForSeconds(5f);
        actionCoroutineAllowed = true;
    }

    void ChangeRandomLoc()
    {
        float random = Random.Range(-0.55f, 4.12f);
        MoveTimer = Random.Range(minumumMovementTimer, maximumMovementTimer);
        randomLoc = new Vector2(random, -1.85f);
    }

    void LogFall()
    {
        animator.SetTrigger("LogFall");
    }

    void LogThrow()
    {
        animator.SetTrigger("LogThrow");
    }

    public void SpawnLogs()
    {
        StartCoroutine(LogEvent());
    }

    public void SpawnLogThrow()
    {
        PlayLogRollingClip();
        Instantiate(logThrowPrefab, logThrowSpawnPos.transform.position, Quaternion.identity);
    }

    IEnumerator LogEvent()
    {
        for (int i = 0; i < 5; i++)
        {
            if(player)
            {
                audioSource.PlayOneShot(warningClip);
                GameObject warning = Instantiate(WarningPrefab, new Vector3(player.transform.position.x, player.transform.position.y + 0.5f, 0f), Quaternion.identity);

                Destroy(warning, 2f);
                GameObject log = Instantiate(logPrefab, transform.position, Quaternion.identity);
                log.GetComponent<LogFall>().SetPosition(new Vector3(player.transform.position.x, 7f, 0f));
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void PlayLogFallClip()
    {
        audioSource.PlayOneShot(logFallClip);
    }

    public void PlayLogThrowClip()
    {
        audioSource.PlayOneShot(logThrowClip);
    }
    public void PlayLogRollingClip()
    {
        audioSource.PlayOneShot(logRollingClip);
    }


}
