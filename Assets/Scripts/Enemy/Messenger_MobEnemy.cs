﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Messenger_MobEnemy : MobEnemy
{
    public EnemyDropData DropData;
    [SerializeField]List<GameObject> Pickups;
    [SerializeField] float dropPercentChance;

    [Header("Bezier Follow")]
    [SerializeField] private List<Transform> routes;
    private int routeToGo;
    private float tParam;
    private Vector2 objectPosition;
    private float speedModifier;
    private bool coroutineAllowed;

    protected override void Start()
    {
        base.Start();

        routeToGo = 0;
        tParam = 0f;
        speedModifier = 0.3f;
        coroutineAllowed = true;
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Route");
        
        foreach(GameObject obj in objs)
        {
            Transform route = obj.GetComponent<Transform>();
            routes.Add(route);
        }
        //Pickups.Add(DropData.ShotgunDrop);
        //Pickups.Add(DropData.HealthDrop);
        //Pickups.Add(DropData.ShieldDrop);
        AddDrops();
        this.OnDeath.AddListener(GetRandomDrop);
    }

    protected override void Update()
    {
        base.Update();

        Move();
    }


    protected override void Die()
    {
        base.Die();
    }

    protected override void Move()
    {
        base.Move();

        if (coroutineAllowed)
        {
            StartCoroutine(GoByTheRoute(routeToGo));
        }
    }

    protected override void OnPlayerCollision(Player player)
    {
        base.OnPlayerCollision(player);
    }

    void GetRandomDrop()
    {
        int randDropChance = Random.Range(1, 100);
        //Debug.Log(randDropChance);
        if(randDropChance <= dropPercentChance)
        {
            Debug.Log("Drop");
            int randomNum = Random.Range(0, Pickups.Count);
            GameObject pickup = Instantiate(Pickups[randomNum], transform.position, Quaternion.identity);
        }
    }

    private IEnumerator GoByTheRoute(int routeNum)
    {
        coroutineAllowed = false;

        Vector2 p0 = routes[routeNum].GetChild(0).position;
        Vector2 p1 = routes[routeNum].GetChild(1).position;
        Vector2 p2 = routes[routeNum].GetChild(2).position;
        Vector2 p3 = routes[routeNum].GetChild(3).position;

        while (tParam < 1)
        {
            tParam += Time.deltaTime * speedModifier;

            objectPosition = Mathf.Pow(1 - tParam, 3) * p0 + 3 * Mathf.Pow(1 - tParam, 2) * tParam * p1 + 3 * (1 - tParam) * Mathf.Pow(tParam, 2) * p2 + Mathf.Pow(tParam, 3) * p3;

            transform.position = objectPosition;
            yield return new WaitForEndOfFrame();
        }

        tParam = 0f;

        routeToGo += 1;

        if (routeToGo > routes.Count - 1)
        {
            routeToGo = 0;
        }

        coroutineAllowed = true;

    }

    void AddDrops()
    {
        Pickups.Add(DropData.HealthDrop);
        if(PlayerPrefs.GetInt("isShotgunPickupUnlocked",0) == 1)
        {
                Pickups.Add(DropData.ShotgunDrop);
        }
        if (PlayerPrefs.GetInt("isShieldPickupUnlocked", 0) == 1)
        {
                Pickups.Add(DropData.ShieldDrop);
        }
        if(PlayerPrefs.GetInt("isBoomerangPickupUnlocked",0) == 1)
        {
            Pickups.Add(DropData.BoomerangDrop);
        }
           
    }
}
