﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Wave Daata", menuName = "WaveData")]
public class WaveData : ScriptableObject
{
    public MobEnemy[] EnemyList;
    public int TotalWave;
}
