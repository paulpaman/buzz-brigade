﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    #region DaniParallax
    private float length;
    private float startPos;

    public float scrollSpeed;

    private void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
        //Debug.Log(length);
    }

    private void Update()
    {
        float x = Mathf.Repeat(Time.time * scrollSpeed, length);
        transform.position = new Vector3(startPos - x, transform.position.y, transform.position.z);
    }

    #endregion

    #region Quad Parallax
    //public float speed;

    //private void Update()
    //{
    //    Vector2 offset = new Vector2(Time.time * speed, 0);

    //    GetComponent<Renderer>().material.mainTextureOffset = offset;
    //}
    #endregion

}
