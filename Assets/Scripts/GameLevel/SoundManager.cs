﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SoundManager : MonoBehaviour
{
    private static readonly string FirstPlay = "FirstPlay";
    private static readonly string BgPref = "BgPref";
    private static readonly string SfxPref = "SfxPref";
    private  int firstPlayInt;

    public Slider bgSlider;
    public Slider sfxSlider;

    private float bgFloat, sfxFloat;

    public AudioSource BGSource;
    public AudioSource[] SFXSources;

    private void Start()
    {
        firstPlayInt = PlayerPrefs.GetInt(FirstPlay);
        if(firstPlayInt == 0)
        {
            bgFloat = 1f;
            sfxFloat = .75f;
            bgSlider.value = bgFloat;
            sfxSlider.value = sfxFloat;

            PlayerPrefs.SetFloat(BgPref, bgFloat);
            PlayerPrefs.SetFloat(SfxPref, sfxFloat);
            PlayerPrefs.SetInt(FirstPlay, -1);
        }
        else
        {
            bgFloat = PlayerPrefs.GetFloat(BgPref);
            bgSlider.value = bgFloat;
            sfxFloat = PlayerPrefs.GetFloat(SfxPref);
            sfxSlider.value = sfxFloat;
        }
    }

    public void SaveSoundSettings()
    {
        PlayerPrefs.SetFloat(BgPref, bgSlider.value);
        PlayerPrefs.SetFloat(SfxPref, sfxSlider.value);
    }

    private void OnApplicationFocus(bool focus)
    {
        if(!focus)
        {
            SaveSoundSettings();
        }
    }

    public void UpdateSound()
    {
        BGSource.volume = bgSlider.value;
        for(int i = 0; i<SFXSources.Length; i++)
        {
            SFXSources[i].volume = sfxSlider.value;
        }
    }
}
