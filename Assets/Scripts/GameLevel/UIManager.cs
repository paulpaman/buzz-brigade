﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UIManager : MonoBehaviour
{
    public Image ShotgunImage;
    public Image ShieldImage;
    public Image BoomerangImage;
    public GameObject ImageParent;
    public GameObject playerObj;
    private Player player;
    Coroutine co;
    public float shotgunTimer;
    private Coroutine currentCoroutine;

    [SerializeField]List<Image> PowerupImages;
    private void Start()
    {
        player = playerObj.GetComponent<Player>();
        
        ImageParent.SetActive(false);
        PowerupImages.Add(ShotgunImage);
        PowerupImages.Add(ShieldImage);
        PowerupImages.Add(BoomerangImage);
        DisableImages();
    }

    public void EnableShotgun(float time)
    {
        DisableImages();
        ShotgunImage.gameObject.SetActive(true);
        StartPattern(AnimatePickup(ShotgunImage, time));
    }

    public void EnableShield(float time)
    {
        DisableImages();
        ShieldImage.gameObject.SetActive(true);
        StartPattern(AnimatePickup(ShieldImage, time));
    }

    public void EnableBoomerang(float time)
    {
        DisableImages();
        BoomerangImage.gameObject.SetActive(true);
        StartPattern(AnimatePickup(BoomerangImage, time));
    }

    public void DisableShotgun()
    {
        ImageParent.SetActive(false);
    }

    public void DisableImages()
    {
        foreach(Image images in PowerupImages)
        {
            images.gameObject.SetActive(false);
            //images.enabled = false;
        }
    }

    void StopPattern()
    {
        if(currentCoroutine != null)
        {
            ImageParent.SetActive(false);
            StopCoroutine(currentCoroutine);
        }
    }

    void StartPattern(IEnumerator aCoroutine)
    {
        StopPattern();
        currentCoroutine = StartCoroutine(aCoroutine);
    }

    IEnumerator AnimatePickup(Image image, float time)
    {
        ImageParent.SetActive(true);
        //Debug.Log("AnimatePickup");
        float CurrentValue = time;
        while(true)
        {
            float fillPercentage = CurrentValue / time;
            //Debug.Log(fillPercentage);
            image.fillAmount = fillPercentage;

            yield return new WaitForSeconds(1f);
            CurrentValue -= 1;
            if (CurrentValue <= 0)
            {
                yield break;
            }
        } 
    }
}
