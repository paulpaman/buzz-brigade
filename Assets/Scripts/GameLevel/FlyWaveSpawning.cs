﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyWaveSpawning : MonoBehaviour
{
    [SerializeField] WaveData[] waveDatas;

    public Transform SpawnPos;

    private int waveIndex;

    [SerializeField] int enemiesKilled;
    [SerializeField] int totalEnemies;
    [SerializeField] float waveSpawnWaitTime;
    [SerializeField] float enemySpawnWaitTime;
    [SerializeField] float waveSpawnPercentChance;

    bool canSpawn;

   
    private void Start()
    {
        canSpawn = true;
        waveIndex = 0;
        totalEnemies = waveDatas[waveIndex].TotalWave;
        //StartCoroutine(SpawnWave());
    }

    private void OnEnable()
    {
        StartCoroutine(SpawnWave());
    }

    private void Update()
    {

    }

    void SpawnEnemy(MobEnemy enemyToSpawn)
    {
        MobEnemy enemy =  Instantiate(enemyToSpawn, SpawnPos.position, Quaternion.identity);
        enemy.OnDeath.AddListener(OnEnemyKill);
    }

    IEnumerator SpawnWave()
    {
        yield return new WaitForSeconds(waveSpawnWaitTime);

        for (int i = 0; i < waveDatas[waveIndex].EnemyList.Length; i++)
        {
            SpawnEnemy(waveDatas[waveIndex].EnemyList[i]);

            yield return new WaitForSeconds(enemySpawnWaitTime);
        }
    }

    IEnumerator CheckSpawn(float time)
    {
        Debug.Log("Check Again");
        int rand = Random.Range(0, 100);
        //Debug.Log(rand);
        yield return new WaitForSeconds(time);
        if (rand <= waveSpawnPercentChance)
        {
            Debug.Log("Spawn Flys");
            StartCoroutine(SpawnWave());
        }
        else
        {
            StartCoroutine(CheckSpawn(8f));
        }

        yield break;
    }

    void OnEnemyKill()
    {
        enemiesKilled++;

        if(enemiesKilled >= totalEnemies)
        {
            waveIndex = Random.Range(0, waveDatas.Length);
           // Debug.Log("Wave Index: " + waveIndex + ("Wave Length: " + waveDatas.Length));
            if (waveIndex < waveDatas.Length)
            {
                enemiesKilled = 0;
                totalEnemies = waveDatas[waveIndex].TotalWave;
                StartCoroutine(CheckSpawn(8f));
            }
        }
    }
}
