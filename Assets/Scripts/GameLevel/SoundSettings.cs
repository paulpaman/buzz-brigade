﻿using UnityEngine;

public class SoundSettings : MonoBehaviour
{
    private static readonly string BgPref = "BgPref";
    private static readonly string SfxPref = "SfxPref";
    private float bgFloat, sfxFloat;

    public AudioSource BGSource;
    public AudioSource[] SFXSources;
    private void Awake()
    {
        ContinueSettings();
    }

    private void ContinueSettings()
    {
        bgFloat = PlayerPrefs.GetFloat(BgPref);
        sfxFloat = PlayerPrefs.GetFloat(SfxPref);

        BGSource.volume = bgFloat;
        for (int i = 0; i < SFXSources.Length; i++)
        {
            SFXSources[i].volume = sfxFloat;
        }
    }
}
