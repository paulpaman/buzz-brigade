﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WaveSpawning : MonoBehaviour
{
    public GameManager gameManager;
    [SerializeField] WaveData[] waveDatas;

    public Transform SpawnPos;

    private int waveIndex;

    [SerializeField] int enemiesKilled;
    [SerializeField] int totalEnemies;
    [SerializeField] float waveSpawnWaitTime;
    [SerializeField] float enemySpawnWaitTime;
    [SerializeField] float waveSpawnPercentChance;

    //[SerializeField] GameObject LevelEndPanel;
    //[SerializeField] LevelEndScript EndScript;

   
    private void Start()
    {
        waveIndex = 0;
        totalEnemies = waveDatas[waveIndex].TotalWave;
        //StartCoroutine(SpawnWave());
    }
    private void OnEnable()
    {
        StartCoroutine(SpawnWave());
    }

    private void Update()
    {

    }

    void SpawnEnemy(MobEnemy enemyToSpawn)
    {
        MobEnemy enemy =  Instantiate(enemyToSpawn, SpawnPos.position, Quaternion.identity);
        if(enemy.isBoss == true)
        {
            enemy.OnBossDeath.AddListener(gameManager.LoadWinScreen);
        }
        enemy.OnDeath.AddListener(OnEnemyKill);
    }

    IEnumerator SpawnWave()
    {
        yield return new WaitForSeconds(waveSpawnWaitTime);

        for (int i = 0; i < waveDatas[waveIndex].EnemyList.Length; i++)
        {
            SpawnEnemy(waveDatas[waveIndex].EnemyList[i]);

            yield return new WaitForSeconds(enemySpawnWaitTime);
        }
    }

    void OnEnemyKill()
    {
        enemiesKilled++;

        if(enemiesKilled >= totalEnemies)
        {
            waveIndex++;
            Debug.Log("Wave Index: " + waveIndex + ("Wave Length: " + waveDatas.Length));
            if (waveIndex < waveDatas.Length)
            {
                enemiesKilled = 0;
                totalEnemies = waveDatas[waveIndex].TotalWave;
                StartCoroutine(SpawnWave());
            }
            else
            {
                return;
            }
        }
    }
}
