﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    float direction;
    float speed;
    float acceleration;
    float curve;
    float dirX;
    float dirY;
    float ttl;
    public float damage;
    private void OnEnable()
    {
        Invoke("Destroy", ttl);
    }

    void Update()
    {
        direction += curve * Time.deltaTime;
        speed += acceleration * Time.deltaTime;

        dirX = xDir(direction);
        dirY = yDir(direction);

        transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x + dirX, transform.position.y + dirY, 0), speed * Time.deltaTime);

        //Check for Out of Bounds
        
    }


    private float xDir(float angle)
    {
        float radians = angle * Mathf.PI / 180;
        return Mathf.Cos(radians);
    }

    private float yDir(float angle)
    {
        float radians = angle * Mathf.PI / 180;
        return -Mathf.Sin(radians);
    }

    public void Destroy()
    {
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    public void SetBehaviors(float x1, float y1, float bulletDirection, float bulletSpeed, float bulletAcceleration, float bulletCurve, float bulletTTL, float bulletDamage)
    {
        direction = bulletDirection;
        speed = bulletSpeed;
        acceleration = bulletAcceleration;
        curve = bulletCurve;
        ttl = bulletTTL;
        damage = bulletDamage;
    }

    private void OnBecameInvisible()
    {
        Destroy();
    }
}
