﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    [SerializeField] private BulletData bulletData;

    [SerializeField] GameObject[] bulletArray;

    int shoot;
    private void Start()
    {
        shoot = bulletData.shoot;
    }

    private void Update()
    {
        SpawnBullet();
    }
    private void SpawnBullet()
    {
        float bulletLength = bulletData.bulletsPerArrays - 1;
        if (bulletLength == 0)
        {
            bulletLength = 1;
        }

        float arrayLength = bulletData.totalBulletArrays - 1 * bulletData.totalBulletArrays;
        if (arrayLength == 0)
        {
            arrayLength = 1;
        }

        float arrayAngle = (bulletData.spreadWithinArray / bulletLength); //Calculates the spread between each array
        float bulletAngle = (bulletData.spreadBetweenArray / arrayLength); //Calcualtes the spread within the bullets in the arrays

        if (shoot == 0) //Fire Rate
        {
            for (int i = 0; i < bulletData.totalBulletArrays; i++)
            {
                for (int j = 0; j < bulletData.bulletsPerArrays; j++)
                {
                    Calculation(i, j, arrayAngle, bulletAngle);
                }
            }
        }

        //If Default Angle > 360 , set it to 0
        float defaultAngle = bulletData.defaultAngle;
        if (defaultAngle > 360)
        {
            defaultAngle = 0;
        }

        defaultAngle += bulletData.spinRate; //Make the pattern spin

        float spinRate = bulletData.spinRate;
        float spinModificator = bulletData.spinModificator;
        spinRate += spinModificator; //Apply the spin modifier

        if (bulletData.invertSpin == true)
        {
            if (spinRate < -bulletData.maxSpinRate || spinRate > bulletData.maxSpinRate)
            {
                spinModificator = -spinModificator;
            }
        }

        shoot +=1;
        Debug.Log(shoot);
        if (shoot >= bulletData.fireRate)
        {
            shoot = 0;
        }
    }
    private void Calculation(int i,int j, float arrayAngle, float bulletAngle)
    {
        float x1 = bulletData.xOffset + lengthDirX(bulletData.objectWidth, bulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + bulletData.startAngle);
        float y1 = bulletData.yOffset + lengthDirY(bulletData.objectWidth, bulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + bulletData.startAngle);

        float direction = bulletData.defaultAngle + (bulletAngle * i) + (arrayAngle * j) + bulletData.startAngle;

        //CreateBullet
        GameObject bullet = BulletPool.bulletPoolInstance.GetBullet();
        bullet.transform.position = new Vector3(this.transform.position.x + x1, this.transform.position.y + y1, 0);
        bullet.transform.rotation = transform.rotation;
        bullet.SetActive(true);
        bullet.GetComponent<Bullet>().SetBehaviors(x1, y1, direction, bulletData.bulletSpeed, bulletData.bulletAcceleration, bulletData.bulletCurve, bulletData.bulletTTL, bulletData.Damage);
    }

    private float lengthDirX(float dist, float angle)
    {
        return dist * Mathf.Cos((angle * Mathf.PI) / 180);
    }

    private float lengthDirY(float dist, float angle)
    {
        return dist * -Mathf.Sin((angle * Mathf.PI) / 180);
    }
}
