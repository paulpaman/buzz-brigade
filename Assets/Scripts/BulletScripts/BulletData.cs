﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Bullet", menuName = "Bullet")]
public class BulletData : ScriptableObject
{
    [Header("Array Settings")]
    //let bulletArray = []; //Every bullets gets stored in here
    public int totalBulletArrays; //Total bullet arrays
    public int bulletsPerArrays; //Bullets per Array

    [Header("Angle Variables")]
    public float spreadBetweenArray; //spread between arrays
    public float spreadWithinArray;//spread between the last and the first bullet of an array
    public float startAngle; //Start angle
    public float defaultAngle;

    [Header("Spinning Variables")]
    public float beginSpinSpeed;
    public float spinRate; // The rate at which the pattern is spinning
    public float spinModificator;// The modificator of the spin rate
    public bool invertSpin; 
    public float maxSpinRate; //The max spin rate ->if SpinRate >= maxSpinRate --> inverts spin

    [Header("Fire Rate Variables")]
    public float fireRate;
    public int shoot;

    [Header("Offsets")]
    public float objectWidth;//Width of the bullet firing object
    public float objectHeight;//Height of the bullet firing object
    public float xOffset;//Shift spawn point of the bullets along the X-Axis
    public float yOffset; //Shift spawn point of the bulltes along the Y-Axis

    [Header("Bullet Variables")]
    public float bulletSpeed;
    public float bulletAcceleration;
    public float bulletCurve;
    public float bulletTTL;
    public int shotsPerAction;

    [Header("Damage")]
    public float Damage;
}
