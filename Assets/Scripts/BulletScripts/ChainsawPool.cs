﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainsawPool : MonoBehaviour
{
    #region Brackeys Tutorial
    //public static EnemyBulletPool bulletPoolInstance;

    //[System.Serializable]
    //public class Pool
    //{
    //    public string tag;
    //    public GameObject prefab;
    //    public int size;
    //}

    //#region Singleton
    //public static EnemyBulletPool Instance;
    //#endregion

    //public List<Pool> pools;
    //public Dictionary<string, Queue<GameObject>> poolDictionary;

    //private void Awake()
    //{
    //    Instance = this;
    //}

    //private void Start()
    //{
    //    poolDictionary = new Dictionary<string, Queue<GameObject>>();

    //    foreach (Pool pool in pools)
    //    {
    //        Queue<GameObject> objectPool = new Queue<GameObject>();

    //        for(int i = 0; i<pool.size; i++)
    //        {
    //            GameObject obj = Instantiate(pool.prefab);
    //            obj.SetActive(false);

    //            objectPool.Enqueue(obj);
    //        }

    //        poolDictionary.Add(pool.tag, objectPool);
    //    }
    //}


    //public GameObject SpawnFromPool(string tag)
    //{
    //    if(!poolDictionary.ContainsKey(tag))
    //    {
    //        Debug.LogWarning("Pool with tag" + tag + "doesn't exist");
    //        return null;
    //    }


    //    GameObject objToSpawn =  poolDictionary[tag].Dequeue();
    //    //objToSpawn.SetActive(true);

    //    poolDictionary[tag].Enqueue(objToSpawn);

    //    return objToSpawn;

    //}
    #endregion

    public static ChainsawPool bulletPoolInstance;

    [SerializeField]
    private GameObject pooledBullet;
    private bool notEnoughBulletsInPool = true;

    private List<GameObject> bullets;

    private void Awake()
    {
        bulletPoolInstance = this;
    }

    private void Start()
    {
        bullets = new List<GameObject>();
    }

    public GameObject GetBullet()
    {
        if (bullets.Count > 0)
        {
            for (int i = 0; i < bullets.Count; i++)
            {
                if (!bullets[i].activeInHierarchy)
                {
                    return bullets[i];
                }
            }
        }

        if (notEnoughBulletsInPool)
        {
            GameObject bullet = Instantiate(pooledBullet);
            bullet.SetActive(false);
            bullets.Add(bullet);
            return bullet;
        }

        return null;
    }
}
