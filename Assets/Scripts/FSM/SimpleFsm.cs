﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFsm : FSM
{
    public enum FSMState
    {
        None,
        PhaseOne,
        PhaseTwo,
        PhaseThree,
        Dead,
    }

    //Current state that the NPC is reaching
    public FSMState curState;

    //Initialize the Finite state machine for the NPC
    protected override void Initialize()
    {
        curState = FSMState.PhaseOne;
    }

    //Update each frame
    protected override void FSMUpdate()
    {
        switch (curState)
        {
            case FSMState.PhaseOne: UpdatePhaseOneState(); break;
            case FSMState.PhaseTwo: UpdatePhaseTwoState(); break;
            case FSMState.PhaseThree: UpdatePhaseThreeState(); break;
            case FSMState.Dead: UpdateDeadState(); break;
        }

        //Update the time
        elapsedTime += Time.deltaTime;

        //Go to dead state is no health left
        //code here
    }

    /// <summary>
    /// Patrol state
    /// </summary>
    protected void UpdatePhaseOneState()
    {

    } 
    
    protected void UpdatePhaseTwoState()
    {

    } 
    
    protected void UpdatePhaseThreeState()
    {

    }

    protected void UpdateDeadState()
    {

    }
   
}
