﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelEndScript : MonoBehaviour
{
    public Text GameScore;
    [Header("Win")]
    public Text WinScoreText;
    public Text WinHighScoreText;
    public GameObject WinPanel;
    [Header("Lose")]
    public Text LoseScoreText;
    public Text LoseHighScoreText;
    public GameObject LosePanel;
    public GameManager gameManager;

    [Header("Level End Panel")]
    public GameObject LevelEndPanel;
    public MenuManager menuManager;
    public AudioSource BgSource;
    public void WinLevel()
    {
        LevelEndPanel.SetActive(true);
        menuManager.enabled = false;
        WinPanel.SetActive(true);
        LosePanel.SetActive(false);
        WinScoreText.text = GameScore.text;
        GetWinHighScore();
        //WinHighScoreText.text = PlayerPrefs
    }

    public void LoseLevel()
    {
        LevelEndPanel.SetActive(true);
        menuManager.enabled = false;
        BgSource.enabled = false;
        LosePanel.SetActive(true);
        WinPanel.SetActive(false);
        LoseScoreText.text = GameScore.text;
        GetLoseScore();
    }

    void GetWinHighScore()
    {
        if (gameManager.CurrentLevel == 1)
        {
            if(PlayerPrefs.GetInt("Level1HighScore",0) < int.Parse(WinScoreText.text))
            {
                PlayerPrefs.SetInt("Level1HighScore", int.Parse(WinScoreText.text));
            }
            WinHighScoreText.text = SavePrefs.Instance.GetLevel1HiScore().ToString();
        }
        else if (gameManager.CurrentLevel == 2)
        {
            if (PlayerPrefs.GetInt("Level2HighScore", 0) < int.Parse(WinScoreText.text))
            {
                PlayerPrefs.SetInt("Level2HighScore", int.Parse(WinScoreText.text));
            }
            WinHighScoreText.text = SavePrefs.Instance.GetLevel2HiScore().ToString();
        }

        else if (gameManager.CurrentLevel == 3)
        {
            if (PlayerPrefs.GetInt("Level3HighScore", 0) < int.Parse(WinScoreText.text))
            {
                PlayerPrefs.SetInt("Level3HighScore", int.Parse(WinScoreText.text));
            }
            WinHighScoreText.text = SavePrefs.Instance.GetLevel3HiScore().ToString();
        }
    }

    void GetLoseScore()
    {
        if (gameManager.CurrentLevel == 1)
        {
            if (PlayerPrefs.GetInt("Level1HighScore", 0) < int.Parse(LoseScoreText.text))
            {
                PlayerPrefs.SetInt("Level1HighScore", int.Parse(LoseScoreText.text));
            }
            LoseHighScoreText.text = SavePrefs.Instance.GetLevel1HiScore().ToString();
        }
        else if (gameManager.CurrentLevel == 2)
        {
            if (PlayerPrefs.GetInt("Level2HighScore", 0) < int.Parse(LoseScoreText.text))
            {
                PlayerPrefs.SetInt("Level2HighScore", int.Parse(LoseScoreText.text));
            }
            LoseHighScoreText.text = SavePrefs.Instance.GetLevel2HiScore().ToString();
        }

        else if (gameManager.CurrentLevel == 3)
        {
            if (PlayerPrefs.GetInt("Level3HighScore", 0) < int.Parse(LoseScoreText.text))
            {
                PlayerPrefs.SetInt("Level3HighScore", int.Parse(LoseScoreText.text));
            }
            LoseHighScoreText.text = SavePrefs.Instance.GetLevel3HiScore().ToString();
        }
    }
}
