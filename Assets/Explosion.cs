﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    float delay;
    Collider2D collider2d;

    [SerializeField] AudioClip audioClip;
    [SerializeField] AudioSource audioSource;
    private void Start()
    {
        audioSource = GameObject.Find("SFX").GetComponent<AudioSource>();
        PlaySound();
        collider2d = gameObject.GetComponent<Collider2D>();
        Destroy(gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + delay);
    }
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if(collision.gameObject.CompareTag("Player"))
    //    {
    //        collider2d.enabled = false;
    //        Player player = collision.gameObject.GetComponent<Player>();
    //        player.TakeDamage();
    //    }
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collider2d.enabled = false;
            Player player = collision.gameObject.GetComponent<Player>();
            if (player.isInvincible == false)
            {
                player.TakeDamage();
            }
        }
    }

    private void PlaySound()
    {
        audioSource.PlayOneShot(audioClip);
    }
}
