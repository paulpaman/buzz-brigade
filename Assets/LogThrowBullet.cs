﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogThrowBullet : MonoBehaviour
{
    public float magnitude;
    public float MinSpeed;
    public float MaxSpeed;
    Vector2 direction;
    Rigidbody2D rb2d;
    // Start is called before the first frame update
    void Start()
    {
        float speed = Random.Range(MinSpeed, MaxSpeed);
        direction = Vector2.one.normalized;
        direction *= new Vector2(speed,-speed);
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.AddRelativeForce(direction * magnitude, ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
