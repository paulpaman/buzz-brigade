﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public Player player;
    public LevelManager levelManager;
    public GameObject UnlockObject;
    public int CurrentLevel;
    public Text ScoreText;
    public LevelEndScript levelEnd;
    private void Start()
    {
        
        Time.timeScale = 1f;
        if(player)
        {
            player.OnPlayerDeath.AddListener(LoadLoseScreen);
        }
        Debug.Log(PlayerPrefs.GetInt("LevelsFinished", 0));
    }

    void LoadLoseScreen()
    {
        Time.timeScale = 0.5f;
        
        StartCoroutine(StartLoseScene());
        Debug.Log("Lose");
    }

    public void LoadWinScreen()
    {
        if(player)
        {
            player.collider.enabled = false;
        }
        Time.timeScale = 0.5f;

        if(CurrentLevel == 1)
        {
            if(PlayerPrefs.GetInt("isShotgunPickupUnlocked", 0) == 0)
            {
                UnlockObject.SetActive(true);
                PlayerPrefs.SetInt("isShotgunPickupUnlocked", 1);
            }
            if (PlayerPrefs.GetInt("LevelsFinished", 0) < 1)
            {
                SavePrefs.Instance.SaveLevel(1);
            }
            //SavePrefs.Instance.SetLevel1HiScore(int.Parse(ScoreText.text));
        }
        else if (CurrentLevel == 2)
        {
            if (PlayerPrefs.GetInt("isShieldPickupUnlocked", 0) == 0)
            {
                UnlockObject.SetActive(true);
                PlayerPrefs.SetInt("isShieldPickupUnlocked", 1);
            }
            if (PlayerPrefs.GetInt("LevelsFinished", 0) < 2)
            {
                SavePrefs.Instance.SaveLevel(2);
            }
            //SavePrefs.Instance.SetLevel2HiScore(int.Parse(ScoreText.text));
        }
        else if(CurrentLevel == 3)
        {
            if (PlayerPrefs.GetInt("isBoomerangPickupUnlocked", 0) == 0)
            {
                UnlockObject.SetActive(true);
                PlayerPrefs.SetInt("isBoomerangPickupUnlocked", 1);
            }
            if (PlayerPrefs.GetInt("LevelsFinished", 0) < 3)
            {
                SavePrefs.Instance.SaveLevel(3);
            }
        }
        
        StartCoroutine(StartWinScreen());
        Debug.Log("Win");
    }

    IEnumerator StartWinScreen()
    {
        yield return new WaitForSecondsRealtime(3f);
        Time.timeScale = 0f;
        levelEnd.WinLevel();
    }

    IEnumerator StartLoseScene()
    {
        yield return new WaitForSecondsRealtime(1f);
        Time.timeScale = 0f;
        levelEnd.LoseLevel();
    }
}
