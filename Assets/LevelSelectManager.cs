﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelSelectManager : MonoBehaviour
{
    [Header("Level1")]
    public Text Level1HiScore;

    [Header("Level2")]
    public Text Level2HiScore;
    public Image Level2Lock;
    public GameObject Level2Panel;

    [Header("Level3")]
    public Text Level3HiScore;
    public Image Level3Lock;
    public GameObject Level3Panel;
    private void OnEnable()
    {
        Debug.Log(PlayerPrefs.GetInt("LevelsFinished", 0));
        LevelChecker();
    }

    public void LevelChecker()
    {
        Level1();
        Level2();
        Level3();
    }

    void Level1()
    {
        Level1HiScore.text = PlayerPrefs.GetInt("Level1HighScore", 0).ToString();
    }

    void Level2()
    {
        if (PlayerPrefs.GetInt("LevelsFinished", 0) > 0)
        {
            Level2Lock.enabled = false;
            Level2Panel.SetActive(true);
            Level2HiScore.text = PlayerPrefs.GetInt("Level2HighScore", 0).ToString();
        }
    }

    void Level3()
    {
        if (PlayerPrefs.GetInt("LevelsFinished", 0) > 1)
        {
            Level3Lock.enabled = false;
            Level3Panel.SetActive(true);
            Level3HiScore.text = PlayerPrefs.GetInt("Level3HighScore", 0).ToString();
        }
    }
}
