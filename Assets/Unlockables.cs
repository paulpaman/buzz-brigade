﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Unlockables : MonoBehaviour
{
    [Header("Shotgun")]
    public Image ShotgunImage;
    public GameObject ShotgunQuestionImage;
    public Text ShotgunText;
    [Header("Shield")]
    public Image ShieldImage;
    public GameObject ShieldQuestionImage;
    public Text ShieldText;

    [Header("Boomerang")]
    public Image BoomerangImage;
    public GameObject BoomerangQuestionImage;
    public Text BoomerangText;



    private void OnEnable()
    {
        CheckPrefs();
    }

    void CheckPrefs()
    {
        if(PlayerPrefs.GetInt("isShotgunPickupUnlocked",0) == 1)
        {
            ShotgunImage.color = Color.white;
            ShotgunQuestionImage.SetActive(false);
            ShotgunText.text = "Shotgun Unlocked";
        }
        if (PlayerPrefs.GetInt("isShieldPickupUnlocked", 0) == 1)
        {
            ShieldImage.color = Color.white;
            ShieldQuestionImage.SetActive(false);
            ShieldText.text = "Shield Unlocked";
        }
        if (PlayerPrefs.GetInt("isBoomerangPickupUnlocked", 0) == 1)
        {
            BoomerangImage.color = Color.white;
            BoomerangQuestionImage.SetActive(false);
            BoomerangText.text = "Boomerang Unlocked";
        }
    }
}
